# packaging sig组贡献积分排行

贡献积分规则参考：[评审规则](sig/packaging/评审规则.md)

贡献积分排行如下：

打包活动贡献排名	
|贡献者|总得分|
|---------|-----------|
zhouganqing2021|222
newwangsong	|56
lllcky	|53
checkmode	|36
handsome_feng	|36
wangsong	|30
liu-yi92	|28
fengyuzhicheng | 24
ctfcc	|23
luoyaoming	|22
snowsi	|18
liweidong1722	|15
tangzhicheng	|13
xiewei	|12
cai-tao1991 | 10
hyter	|10
hanteng	|10
dunto	|10
TonyMi	|10
allen-ukui	|8
LinKuiyi	|8
seven_leaves_snow	|5
larue	|5
ky-ywx	|5
hantengc | 1
zyhaozi | 1
