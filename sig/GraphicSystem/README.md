# GraphicSystem SIG

负责GraphicSystem开源软件包的维护。发布openKylin的GraphicSystem 版本，进行软件包构建、系统构建等工作。

## 工作目标

- 负责 openKylin GraphicSystem 版本的规划、制作、维护和升级
- 负责GraphicSystem同x86、ARM间的应用兼容技术探索

## 邮件列表

graphicsystem@lists.openkylin.top

## SIG成员

### Owner

- [ld5m@163.com](https://gitee.com/ld5m@163.com)

### Maintainers

- yanglingyun@linzhuotech.com
- yanggaofin@linzhuotech.com
- zhangxz_cetc15@cetc.com

### Committers

## 仓库列表
- [GraphicSystem](https://gitee.com/openkylin/graphicsystem)

