# 操作系统智能化兴趣小组 (SIG)

AI4OS：操作系统智能化（Artificial Intelligence for Operating System）致力于将人工智能（AI）与操作系统（OS）相结合，以实现操作系统的智能化和性能优化。将大模型为代表的AI技术嵌入openKylin操作系统，让AI深扎底层操作系统，可以在没有任何应用作为中介的情况下，直接调用AI大模型能力完成任务。

## 工作目标

AI4OS的目标是通过结合AI和操作系统，解决以下关键问题：

- 性能优化：利用AI技术来提高操作系统的性能，减少资源占用和响应时间，以更高效地运行应用程序。

- 用户体验改进：通过智能化的界面和功能，提供更流畅、个性化和用户友好的操作系统体验。

## SIG 成员

### Owner

- lizhzhuo (lizhuo@yhkylin.cn)

### Maintainers

- lizhzhuo (lizhuo@yhkylin.cn)

## SIG 维护包列表

- [ncnn](https://gitee.com/Tencent/ncnn)

- [paddle-lite](https://gitee.com/paddlepaddle/paddle-lite)

- [mnn](https://gitee.com/mirrors/mnn)

## SIG 邮件列表

- [ai4os@lists.openkylin.top](mailto:ai4os@lists.openkylin.top)

## SIG 组例会

月会
